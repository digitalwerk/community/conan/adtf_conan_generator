# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from conans import ConanFile, tools, __version__ as conan_version
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps
from conans.errors import ConanException
import os
import re

class Build(ConanFile):
    name = "adtf_conan_generator"
    label = "ADTF Conan Generator"
    description = "Header only package to generate startup scripts and adtfenvironment file"
    url = "https://gitlab.com/digitalwerk/solutions/conan/adtf_conan_generator.git"
    homepage = "https://gitlab.com/digitalwerk/solutions/conan/adtf_conan_generator"
    license = "MPL-2.0"
    settings = "os", "arch", "compiler", "build_type"

    keep_imports = True

    tool_requires = ["cmake/3.23.2@dw/stable",
                     "doxygen/1.9.1@fep/stable",
                     "graphviz/2.47.3@fep/stable"]

    def generate(self):
        tc = CMakeToolchain(self)

        if re.match(r'^([\d]+).([\d]+).([\d]+)$', str(self.version)):
            version_string = self.version
        else:
            version_string = '1.99.99'

        tc.cache_variables["LABEL_NAME"] = self.label
        tc.cache_variables["BUILD_VERSION"] = version_string
        tc.cache_variables["GIT_COMMIT"] = os.environ.get("CI_COMMIT_SHA", "local")

        crosscompiling_emulator = os.environ.get("CONAN_CROSSCOMPILING_EMULATOR")
        if crosscompiling_emulator:
            tc.cache_variables["CMAKE_CROSSCOMPILING_EMULATOR"] = str(crosscompiling_emulator)
        tc.cache_variables["CMAKE_CONFIGURATION_TYPES"] = str(self.settings.build_type)
        tc.cache_variables["CMAKE_INSTALL_MESSAGE"] = "NEVER"

        tc.generate()

        deps = CMakeDeps(self)

        deps.generate()

    def build(self):
        cmake = CMake(self)

        if self.should_configure:
            cmake.configure()

        if self.should_build:
            cmake.build()

        if self.should_install:
            cmake.install()

        if self.should_test:
            ctest_call = "ctest -C " + str(self.settings.build_type) + " -T test --no-compress-output --output-on-failure --output-junit test_results.junit.xml"
            self.run(ctest_call, cwd=self.build_folder, ignore_errors=True)

    def package(self):
        pass

    def package(self):
        pass
