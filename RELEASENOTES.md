# Release Notes

## New Features & Changes

- **1.6.0**
  - Support self contained packages
  - Show the usage of macros
  - Option to generate launch and tool options
  - Add Profiling shell scripts
- **1.5.0**
  - Provide possibility to use absolute paths in generated scripts
  - Provide a complex example
- **1.4.0**
  - Add default name for overall environment file
  - Create a platform, compiler and build type independent package
- **1.3.0**
  - Start launcher without --console argument
  - Forward script args
  - Added get_os_current_script_dir() to all environment paths
  - Make generator more configurable by the user
- **1.2.0**
  - Make the generator able to be called implicitly by a conan-recipe
- **1.1.0**
  - Assign environment directory macro
  - Change recursive environment discovery to top level environment file only
- **1.0.0**
  - Initial version to generate start scripts related to required adtf dependencies
  - Ability to create and deploy adtf sessions

---

## Bugfixes

- **1.6.0**
  - Add release notes
- **1.5.0**
  - Fill readme with content
  - Change conan generator version in example
  - Create documentation explaining how to structure your conan setup
- **1.4.0**
  - Removed extra .adtfenvironment file for build type
- **1.3.0**
  - none
- **1.2.0**
  - none
- **1.1.0**
  - Add missig license to repository
- **1.0.0**
  - none (initial version)

---

## Known Issues

- self.user_info.ADTF3_ENVIRONMENT_FILES does not support package-relative paths
- Additional environment file path must be absolute
